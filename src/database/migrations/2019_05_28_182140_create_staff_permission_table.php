<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_permission', function (Blueprint $table) {
            $table->unsignedbigInteger('staff_id');
            $table->string('permission_id');

            $table->foreign('staff_id')
                ->references('id')->on('staff')->onDelete('cascade');
            $table->foreign('permission_id')
                ->references('id')->on('permissions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_permission');
    }
}
