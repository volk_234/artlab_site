<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Utils\RequestedScope;
use Carbon\Carbon;

class Schedule extends RequestedScope
{
    protected $fillable = ['lesson_id', 'location', 'date',
        'day', 'open_time', 'close_time'];

    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }

    public function getOpenTimeFormattedAttribute()
    {
        return Carbon::parse($this->open_time)->tz('Asia/Yekaterinburg')->format('H:i');
    }

    public function getCloseTimeFormattedAttribute()
    {
        return Carbon::parse($this->close_time)->tz('Asia/Yekaterinburg')->format('H:i');
    }

    static public function monthSchedules()
    {
        $schedules = Schedule::with('lesson')->get()->groupBy('day');
        $period = Carbon::today()->range(Carbon::now()->lastOfMonth()->endOfDay());
        for ($i = -1; $i < 7; $i++) {
            $schedules[$i] = $schedules[$i] ?? collect();
        }


        $iterator = $period->map(function (Carbon $item) use ($schedules) {
            $datedSchedules = $schedules[-1]->where('date', $item->toDateString());
            return collect([
                'date' => $item->locale('ru')->isoFormat('D MMMM'),
                'dayName' => $item->dayName,
                'schedules' => $datedSchedules->merge($schedules[$item->dayOfWeek])
            ]);
        });
        $dates = iterator_to_array($iterator);
        return $dates;
    }
}