<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 31.05.2019
 * Time: 19:46
 */

namespace App\Models;

use App\Utils\RequestedScope;
use Illuminate\Database\Eloquent\Model;

class Permission extends RequestedScope
{
    public $incrementing = false;

    protected $fillable = ['id', 'name'];
    public $timestamps = null;

    public const USERS_VIEW = 'users.view';
}