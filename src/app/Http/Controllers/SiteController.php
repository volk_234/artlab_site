<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers;


use App\Models\Schedule;
use App\Models\Trainer;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class SiteController extends Controller
{

    public function index()
    {
        $schedules = Schedule::monthSchedules();
        $currentMonth = Carbon::now()->monthName;
        return view('index', [
            'scheduleDates' => $schedules,
            'currentMonth' => $currentMonth,
        ]);
    }

    public function schedule(Request $r)
    {
        $schedules = Schedule::monthSchedules();

        $today = Carbon::today('Asia/Yekaterinburg');

        $date = $r->date ? Carbon::parse($r->date, 'Asia/Yekaterinburg') : $today;

        $carbonPeriod = $today->daysUntil(Carbon::today('Asia/Yekaterinburg')->endOfMonth());
        $sliderDates = [];
        foreach ($carbonPeriod as $d) {
            array_push($sliderDates, [
                'iso' => $d->format('d-m-Y'),
                'dayOfWeek' => $d->formatLocalized('%A'),
                'date' => $d->formatLocalized('%d %B'),
                'active' => $d->diffInDays($date) ? false : true
            ]);
        }

        return view('schedule', [
            'schedules' => Schedule::schedulesForDate($date, $r->group),
            'sliderDates' => $sliderDates
        ]);
    }
}