<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Middleware\CheckAdminAccess;
use App\Http\Requests\StoreUser;
use App\Models\User;
use Illuminate\Support\Str;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:api', CheckAdminAccess::class]);
    }

    public function index()
    {
        $users = User::all();
        return ['response' => $users];
    }

    public function store(StoreUser $request)
    {
        $user = new User();
        $user->fill($request->all());

        $password = Str::random(6);
        $user->password = bcrypt($password);

        $user->save();

        return ['password' => $password];
    }
}