<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 22.08.2019
 * Time: 17:32
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Middleware\CheckAdminAccess;
use App\Models\Lesson;
use Illuminate\Http\Request;

class LessonController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:api', CheckAdminAccess::class]);
    }

    public function index(Request $r)
    {
        $result = Lesson::requested($r)->get();
        return ['response' => $result];
    }

    public function store(Request $r)
    {
        $this->validate($r, [
            'name' => 'string|required|max:255',
            'image_id' => 'sometimes|nullable|exists:media_files,id',
        ]);

        $lesson = Lesson::create($r->all());

        if($r->has('schedules')){
            $lesson->schedules()->delete();
            foreach ($r->schedules as $schedule){
                $lesson->schedules()->create($schedule);
            }
        }

        return ['response' => $lesson];
    }

    public function show(Request $r, Lesson $lesson)
    {
        $lesson->loadRequested($r);
        return ['response' => $lesson];
    }

    public function destroy(Request $r, Lesson $lesson)
    {
        $lesson->delete();
        return ['response' => 1];
    }

    public function update(Request $r, Lesson $lesson)
    {
        $lesson->update($r->only($lesson->getFillable()));

        if($r->has('schedules')){
            $lesson->schedules()->delete();
            foreach ($r->schedules as $schedule){
                $lesson->schedules()->create($schedule);
            }
        }

        return ['response' => $lesson];
    }
}