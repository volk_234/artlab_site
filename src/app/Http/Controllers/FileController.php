<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class FileController extends Controller
{
    public function show(Request $r, Asset $file)
    {
        return response(['response' => $file]);
    }

    public function upload(Request $r)
    {
        $file = null;
        switch ($r->type) {
            case 'file':
                $this->validate($r, [
                    'file' => 'file | max:4096 | mimes:jpeg,jpg,png'
                ]);

                $thumbnail = null;
//                $this->optimizeIfNeed($r->file);
//                $thumbnail = $this->makeThumbnailIfNeed($r->file);

                //фикс бага, когда symfony убирает расширение у SVG файла
                $name = explode('.', $r->file->hashName())[0];
                $storedFile = $r->file('file')->storeAs(
                    '/public/files', $name . '.' . $r->file->getClientOriginalExtension());

                $file = Asset::create([
                    'type' => $r->file->getMimeType(),
                    'original' => Storage::url($storedFile),
                    'thumbnail' => $thumbnail ? Storage::url($thumbnail) : null,
                ]);

                break;
            case 'video':
                $matches = [];
                preg_match('/src="(.*?)"/', $r->file, $matches);
                $file = Asset::create([
                    'type' => 'video',
                    'original' => $matches[1],
                ]);
                break;
        }
        return response(['response' => $file]);
    }

    private function makeThumbnailIfNeed(UploadedFile $file)
    {
        $validator = Validator::make(['file' => $file], [
            'file' => 'mimes:jpeg,bmp,png,gif'
        ]);

        if ($validator->fails()) {
            return null;
        }

        $fileName = pathinfo($file->hashName(), PATHINFO_FILENAME);
        $thumbnailPath = 'public/files/' . $fileName . '_thumbnail.png';

        $thumbnail = Image::make($file)->resize(300, 200);

        Storage::put($thumbnailPath, (string)$thumbnail->encode());

        return $thumbnailPath;
    }

    private function optimizeIfNeed(UploadedFile $file)
    {
        $validator = Validator::make(['file' => $file], [
            'file' => 'mimes:jpeg,png,svg,gif'
        ]);

        if ($validator->fails()) {
            return null;
        }

        return ImageOptimizer::optimize($file);
    }
}
