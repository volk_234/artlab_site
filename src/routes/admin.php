<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 25.08.2019
 * Time: 1:35
 */


use App\Http\Middleware\CheckAdminAccess;
use Illuminate\Support\Facades\Route;

Route::redirect('/admin', '/admin/lessons');

Route::get('/admin/{any}', 'Admin\SiteController@index')->where('any', '.*');
