import Swiper from "swiper/dist/js/swiper";

export const initSwipers = function () {
    let swipers = document.getElementsByClassName('swiper-container');

    for (let i = 0; i < swipers.length; i++) {
        let item = swipers[i];
        let spaceBetween = item.getAttribute("data-spaceBetween") || 0;
        let preset = parseInt(item.getAttribute("data-preset") || 0);

        switch (preset) {
            case 1:
                let initialSlideNode = item.querySelector('.date-slider-item.active').parentNode;
                let initialSlide = [...initialSlideNode.parentNode.children].indexOf(initialSlideNode);
                new Swiper(item, {
                    direction: 'horizontal',
                    slidesPerView: 'auto',
                    freeMode: true,
                    mousewheel: true,
                    releaseOnEdges: false,
                    initialSlide: initialSlide,
                });
                break;
            default:
                new Swiper(item, {
                    direction: 'horizontal',
                    autoplay: true,
                    delay: 10000,
                    spaceBetween: spaceBetween,
                    loop: true,
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true
                    },
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    observer: true,
                });
                break;
        }

    }
};
