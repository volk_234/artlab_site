import axios from 'axios';
import moment from 'moment';
import {toCamelCaseKeys} from "../utils/toCamelCase";

axios.defaults.transformResponse = [(data) => {
    data = JSON.parse(data);
    return toCamelCaseKeys(data, {deep: true});
}];

axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (401 === error.response.status) {
        window.location = '/login';
    } else {
        return Promise.reject(error);
    }
});

export let lessons = {
    fetch: function (params) {

        return new Promise((resolve, reject) => {
            axios
                .get(`/api/admin/lessons`, {params: params})
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    get: function (id, params) {
        return new Promise((resolve, reject) => {
            axios
                .get(`/api/admin/lessons/${id}`, {params: params})
                .then(response => {
                    let lesson = response.data.response;
                    if (lesson.schedules) {
                        lesson.schedules.forEach((item) => {
                            item.openTime = moment.utc(item.openTime, 'HH:mm').local().format('HH:mm:ss');
                            item.closeTime = moment.utc(item.closeTime, 'HH:mm').local().format('HH:mm:ss');
                        });
                    }

                    resolve(lesson);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    store: function (params) {
        params.schedules.forEach((item) => {
            item.open_time = moment(item.open_time, 'HH:mm').utc().format('HH:mm:ss');
            item.close_time = moment(item.close_time, 'HH:mm').utc().format('HH:mm:ss');
        });

        return new Promise((resolve, reject) => {
            axios
                .post(`/api/admin/lessons`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    update: function (id, params) {
        params.schedules.forEach((item) => {
            item.open_time = moment(item.open_time, 'HH:mm').utc().format('HH:mm:ss');
            item.close_time = item.close_time ? moment(item.close_time, 'HH:mm').utc().format('HH:mm:ss') : null;
        });

        return new Promise((resolve, reject) => {
            axios
                .put(`/api/admin/lessons/${id}`, params)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
    destroy: function (id) {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/api/admin/lessons/${id}`)
                .then(response => {
                    resolve(response.data.response);
                })
                .catch((e) => {
                    reject(e.response)
                });
        });
    },
};
