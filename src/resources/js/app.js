import Swiper from 'swiper/dist/js/swiper';
import AOS from 'aos';

let schedulesSwiper = new Swiper('.slider-schedules', {
    direction: 'vertical',
    slidesPerView: 'auto',
    freeMode: true,
    scrollbar: {
        el: '.swiper-scrollbar',
    },
    mousewheel: true,
    breakpoints: {
        direction: 'vertical',
        960: {
            slidesPerView: 1
        }
    }
});


new Swiper('.slider-news', {
    slidesPerView: 2,
    spaceBetween: 30,
    breakpoints: {
        960: {
            slidesPerView: 1
        }
    }
});

let forms = document.getElementsByClassName('form-ajax');
for (let form of forms) {
    form.addEventListener('submit', function (e) {
        e.preventDefault();
        this.classList.toggle('form-ajax_submitted', true);
        return false;
    });
}

AOS.init();
