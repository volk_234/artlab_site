import VueRouter from "vue-router";
import Vue from "vue";

import TheLessons from "../components/admin/TheLessons";
import TheLessonEditor from "../components/admin/TheLessonEditor";

Vue.use(VueRouter);

let routes = [
    {
        component: TheLessons,
        path: '/admin/lessons',
        name: 'lessons',
        props: true,
        meta: {title: 'Занятия | Панель управления'}
    },
    {
        component: TheLessonEditor,
        path: '/admin/lessons/create',
        name: 'lessons.create',
        props: true,
        meta: {title: 'Создание занятия | Панель управления'}
    },
    {
        component: TheLessonEditor,
        path: '/admin/lessons/:lessonId/edit',
        name: 'lessons.edit',
        props: true,
        meta: {title: 'Редакторование занятия | Панель управления'}
    },
];

export const router = new VueRouter({
    mode: 'history',
    routes: routes
});

router.beforeEach((to, from, next) => {
    document.title = to.meta.title || document.title;
    next();
});
