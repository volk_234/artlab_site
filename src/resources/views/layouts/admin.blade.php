<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="yandex-verification" content="6ec22a12ac322d90"/>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link href="{{mix('css/vendor.css')}}" rel="stylesheet">
    <link href="{{mix('css/app.css')}}" rel="stylesheet">
</head>
<body class="d-flex flex-column">
<div class="sidebar-page" id="app">
    <aside class="sidebar">
        <div class="sidebar__header">Панель управления</div>
        <ul>
            <li>
                <router-link to="/admin/lessons">
                    Занятия
                </router-link>
            </li>
            <li>
                <a href="/">Вернуться на сайт</a>
            </li>
            <li>
                <a href="/logout">Выйти из панели управления</a>
            </li>
        </ul>
    </aside>
    <div class="sidebar-page__content">
        <router-view></router-view>
    </div>
</div>
<script src="{{mix('js/manifest.js')}}"></script>
<script src="{{mix('js/vendor.js')}}"></script>
<script src="{{mix('js/app.js')}}"></script>
<script src="{{mix('js/admin.js')}}"></script>
</body>
</html>
