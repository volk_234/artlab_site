<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="{{mix('/css/vendor.css')}}">
    <link rel="stylesheet" href="{{mix('/css/app.css')}}">
    <title>Лаборатория технического творчества СурГУ</title>
</head>
<body>
<div class="block-entry">
    <nav class="nav position-relative z5">
        <div class="container d-flex align-items-center">
            <a href="/">
                <img class="mr-3" src="/images/logo.svg">
            </a>
            <div class="nav__links">
                <a class="mr-3" href="#clubs">КЛУБЫ</a>
                <a class="mr-3" href="#schedule">РАСПИСАНИЕ</a>
                <a class="mr-3" href="#news">НОВОСТИ</a>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row align-items-center pb-5">
            <div class="z5 col-lg-5 col-12 text-right order-2"
                 data-aos="fade-left">
                <div class="h3">
                    Лаборатория
                    <div class="h1 text-secondary mb-0 d-flex justify-content-end">технического
                        творчества
                    </div>
                    Сургу
                </div>
            </div>
            <div class="col-lg-7 col-12 order-1 order-lg-3">
                <div class="entry-block-lamp__bg"></div>
                <div class="entry-block-lamp__svg-wrap">
                    @include('components.lamp')
                </div>
            </div>
        </div>
        <div class="row py-7 align-items-center">
            <div class="col-12 col-lg-6 position-relative">
                @include('components.star')
                <img class="w-100 w-lg-auto" src="/images/meets.svg">
            </div>
            <div class="col-12 col-lg-6"
                 data-aos="fade-right">
                <h2 class="text-secondary">находим
                    единомышленников</h2>
                <p>
                    Лаборатория технического творчества создана для общения и совсместного времяпровождения людей,
                    связанных общими
                    интересами.
                    Инициатором клуба может быть кто угодно - студент, выпускник, сотрудник университета или любой
                    другой организации.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container mt-n5 pb-5">
    <div class="row align-items-center">
        <div class="col-12 col-md-6 order-2 order-md-0"
             data-aos="fade-left">
            <h2>Развиваемся вместе</h2>
            <p>На встречах (с чаем и печеньками) в свободном формате обсуждаются интересные материалы и
                познаются новые практики. Иногда приглашаются сотрудники сторонних организаций, которые
                рассказывают о современных трендах и отвечают на возникшие вопросы.</p>
        </div>
        <div class="col-12 col-md-6 order-1">
            <img class="w-100" src="/images/study-together.svg">
        </div>
    </div>
</div>
<a name="clubs"></a>
<div class="clubs py-5 z5 position-relative">
    <div class="container"
         data-aos="fade-up">
        <h2 class="text-center mb-4">7 студенческих клубов</h2>
        <div class="row">
            <div class="col-12 col-md-6 col-lg-3 mb-4">
                <div class="clubs-item">
                    <div class="clubs-item__header">
                        @include('components.club-web-img')
                        <div class="clubs-item__title">Мейкеры</div>
                    </div>
                    <div class="clubs-item__description">
                        Тут должен быть текстТут должен быть текстТут должен быть
                        текстТут должен быть текстТут должен
                        быть текст
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mb-4">
                <div class="clubs-item">
                    <div class="clubs-item__header">
                        @include('components.club-web-img')
                        <div class="clubs-item__title">CTF</div>
                    </div>
                    <div class="clubs-item__description">
                        Тут должен быть текстТут должен быть текстТут должен быть
                        текстТут должен быть текстТут должен
                        быть текст
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mb-4">
                <div class="clubs-item">
                    <div class="clubs-item__header">
                        @include('components.club-web-img')
                        <div class="clubs-item__title">WEB</div>
                    </div>
                    <div class="clubs-item__description">
                        Тут должен быть текстТут должен быть текстТут должен быть
                        текстТут должен быть текстТут должен
                        быть текст
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mb-4">
                <div class="clubs-item">
                    <div class="clubs-item__header">
                        @include('components.club-web-img')
                        <div class="clubs-item__title">Инкубатор</div>
                    </div>
                    <div class="clubs-item__description">
                        Тут должен быть текстТут должен быть текстТут должен быть
                        текстТут должен быть текстТут должен
                        быть текст
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mb-4">
                <div class="clubs-item">
                    <div class="clubs-item__header">
                        @include('components.club-web-img')
                        <div class="clubs-item__title">Хвостовики</div>
                    </div>
                    <div class="clubs-item__description">
                        Тут должен быть текстТут должен быть текстТут должен быть
                        текстТут должен быть текстТут должен
                        быть текст
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mb-4">
                <div class="clubs-item">
                    <div class="clubs-item__header">
                        @include('components.club-web-img')
                        <div class="clubs-item__title">StudTalks</div>
                    </div>
                    <div class="clubs-item__description">
                        Тут должен быть текстТут должен быть текстТут должен быть
                        текстТут должен быть текстТут должен
                        быть текст
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 mb-4">
                <div class="clubs-item">
                    <div class="clubs-item__header">
                        @include('components.club-web-img')
                        <div class="clubs-item__title">OpenSource</div>
                    </div>
                    <div class="clubs-item__description">
                        Тут должен быть текстТут должен быть текстТут должен быть
                        текстТут должен быть текстТут должен
                        быть текст
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a name="schedule"></a>
<div class="schedules py-5"
     data-aos="fade-up">
    <div class="container">
        <h2 class="text-center">Расписание на <span class="text-primary">{{$currentMonth}}</span></h2>
        <div class="swiper-container slider-schedules">
            <div class="swiper-wrapper">
                @foreach($scheduleDates as $scheduleDate)
                    <div class="swiper-slide">
                        <div class="row">
                            <div class="col-12 col-md-2 h5">
                                {{$scheduleDate['dayName']}}<br/>
                                {{$scheduleDate['date']}}
                            </div>
                            <div class="col-12 col-md-10">
                                <div class="row">
                                    @foreach($scheduleDate['schedules'] as $schedule)
                                        <div class="col-12 col-sm-6 col-lg-4 col-xl-3 mb-4">
                                            <div class="schedules-item">
                                                <div class="text-secondary mr-2 text-right">
                                                    <div class="schedules-item__time">
                                                        {{$schedule->open_time_formatted}}
                                                    </div>
                                                    <div class="schedules-item__cabinet">
                                                        {{$schedule->location}}
                                                    </div>
                                                </div>
                                                <div>{{$schedule->lesson['name']}}</div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="swiper-scrollbar"></div>
        </div>
    </div>
</div>
<a name="news"></a>
<div class="news py-5">
    <div class="container"
         data-aos="fade-up">
        <h2>Последние новости</h2>
        <div class="mb-5">
            <div class="swiper-container slider-news">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="news-item">
                            <div class="h5 news-item__header">
                                <div class="news-item__date">
                                    <div class="h2 news-item-date__digit">22</div>
                                    ноября
                                </div>
                                <div class="news-item__title">
                                    Встреча клуба "Отражение"
                                </div>
                            </div>
                            <div class="news-item__description">
                                <p>Новый дом сургутского фотоклуба "Отражение" 21 ноября в очередной раз собрал
                                    любителей фотографии.
                                </p>
                                <p>Следующая встреча 12 декабря.<br>
                                    Адрес: СурГУ, УНИКИТ, ул. Энергетиков 22, ауд. 905 - "Лаборатория технического
                                    творчества СурГУ"</p>
                            </div>
                            {{--<a href="#" class="news-item__btn btn btn_light">Читать</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="text-right">--}}
        {{--<a href="#" class="btn btn-secondary">--}}
        {{--смотреть весь архив новостей</a>--}}
        {{--</div>--}}
    </div>
</div>
<footer>
    <div class="container text-center">
        <div class="mb-4">По всем вопросам, замечаниям и предложениям:</div>
        <div class="mb-4">Федоров Дмитрий Алексеевич<br/>
            <a href="mailto:fda.polytech@gmail.com">fda.polytech@gmail.com</a>
        </div>
        <div class="mb-4">Кафедра Информатики и вычислительных систем</div>
    </div>
</footer>
<script src="{{mix('/js/manifest.js')}}"></script>
<script src="{{mix('/js/vendor.js')}}"></script>
<script src="{{mix('/js/app.js')}}"></script>
</body>
</html>