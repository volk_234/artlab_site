<svg class="star"
     data-aos="star"
     data-aos-anchor-placement="top-top"
     viewBox="0 0 810 771" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M405.053 0.343903L500.63 294.501L809.926 294.501L559.701 476.301L655.278 770.458L405.053 588.659L154.827 770.458L250.405 476.301L0.179535 294.501L309.475 294.501L405.053 0.343903Z"
          fill="url(#paint0_linear)"/>
    <defs>
        <linearGradient id="paint0_linear" x1="405.053" y1="0.343903" x2="405.053" y2="851.762"
                        gradientUnits="userSpaceOnUse">
            <stop stop-color="#FFFB39"/>
            <stop offset="1" stop-color="#FDFFA6" stop-opacity="0.77"/>
        </linearGradient>
    </defs>
</svg>
