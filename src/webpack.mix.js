const webpack = require('webpack');
const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix
    .webpackConfig({
        plugins: [
            new webpack.ContextReplacementPlugin(
                /moment[\/\\]locale/,
                // A regular expression matching files that should be included
                /(ru)\.js/
            )
        ]
    })
    .setPublicPath('public')
    .options({processCssUrls: false})
    .version()
    .js('resources/js/app.js', 'public/js')
    .js('resources/js/admin.js', 'public/js')
    .extract(['axios', 'vue', 'swiper', 'aos', 'moment'])
    .sass('resources/sass/vendor.scss', 'public/css')
    .sass('resources/sass/app.scss', 'public/css');