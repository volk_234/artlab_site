cp src/.env.example src/.env && \
echo "NGINX_PORT=8080:" > .env && \
docker-compose up -d -p artlab && \
docker exec artlab_app_1 php artisan migrate